<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Sous Laravel 5.6

Laravel est un framework d'application web avec une syntaxe expressive et élégante. Nous croyons que le développement doit être une expérience agréable et créative pour être vraiment enrichissante. Laravel tente de soulager la douleur du développement en facilitant les tâches communes utilisées dans la majorité des projets web.

## Git tardif ?

Pour ne pas perdre trop de temps, j'ai préféré ne pas initialiser Git from scratch.

## Reste à configurer

Renseigner une adresse mail et un mot de passe valide dans .env

Et puis l'habituel :
`composer install`

Ainsi que
`php artisan migrate`

## Optionnel

J'ai ajouté un dump de ma base de données au cas où (dump.sql)

Pour toute autres questions, <a href="mailto:rocherv.valentin@gmail.com">rocherv.valentin@gmail.com</a>
