-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 11 juil. 2018 à 23:46
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `anoblog`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_user_id_foreign` (`user_id`),
  KEY `comments_post_id_foreign` (`post_id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `content`, `user_id`, `post_id`, `created_at`, `updated_at`) VALUES
(1, 'Cool !', 1, 1, '2018-07-10 17:52:35', '2018-07-10 17:52:35'),
(2, 'Tip Top', NULL, 1, '2018-07-10 17:53:20', '2018-07-10 17:53:20'),
(3, 'Enfin une bonne nouvelle', 1, 1, '2018-07-10 18:08:19', '2018-07-10 18:08:19'),
(4, 'Nice', 1, 1, '2018-07-10 19:00:48', '2018-07-10 19:00:48'),
(34, 'Good news, it\'s work', 1, 1, '2018-07-11 13:55:24', '2018-07-11 13:55:24'),
(36, 'Finalement', NULL, 1, '2018-07-11 15:58:43', '2018-07-11 15:58:43');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2018_07_07_195356_create_posts_table', 1),
(8, '2018_07_09_124336_create_comments_table', 2);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `content`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Nouvelles flottes de véhicules chez Hager', 'nouvelles-flottes-de-vehicules-chez-hager', 'Hager vient de l\'annoncer, leur usine d\'Obernai va s’équiper de nouveaux véhicules à destinations de ses employés itinérants.\r\nDepuis le dernier mondial de l’automobile, l’actualité est aux véhicules électriques.\r\nAvec plus de 90 000 immatriculations en 2016, la France représente 30 % des ventes de véhicules électriques en Europe. Elle constitue donc le 1er marché européen devant la Norvège (27 %) et l’Allemagne (15 %) et fait également partie du Top 3 mondial. Sur un marché européen en pleine expansion (+38% en 3 ans), la France tient une place importante dans le développement du parc européen de véhicules électriques.\r\n\r\nUn marché en expansion\r\n\r\nGrâce aux aides gouvernementales et en dépit de la baisse des prix du pétrole, les ventes de véhicules électriques progressent dans l’hexagone et se rapprochent de la barre symbolique des 1% du marché, avec une vente de plus de 23% en France en 2015. Au total 27 307 voitures particulières électriques neuves ont été immatriculées en 2015 en France, dans un marché de 2 millions d’unités*.\r\n\r\n*Statistiques du Comité des Constructeurs Français d’Automobiles (CCFA)\r\n\r\nEn effet, fort de l’enjeu économique considérable, la politique d’incitation gouvernementale se manifeste par un soutien financier au déploiement des infrastructures de charge et par un soutien à l’industrie automobile.\r\nEn corrélation avec le développement de la législation, l’augmentation des ventes, le développement des flottes d’auto-partages électriques et l’acquisition de véhicules électriques par les entreprises et les administrations, indiquent un tournant pour la mobilité électrique. Un tournant que l’entreprise HAGER a anticipé.', 1, '2018-07-08 13:38:29', '2018-07-11 11:38:19'),
(2, 'Nouveau modèle de Tesla', 'nouveau-modele-de-tesla', 'Le constructeur vient d\'annoncer un nouveau modèle de voiture équipé de son auto-pilot, le model D', 1, '2018-07-08 13:48:48', NULL),
(5, 'La géolocalisation pour sauver des vies', 'la-geolocalisation-pour-sauver-des-vies', 'Conducteurs, randonneurs, simples promeneurs, chacun d’entre nous a eu l’occasion de bénir les inventeurs du GPS ! Ce système performant de géolocalisation facilite la vie.\r\n\r\nLes entreprises utilisant plusieurs véhicules en ont découvert une nouvelle application : le suivi de flotte. Un simple boîtier GPS connecté à un modem GSM GPRS peut être installé discrètement dans un véhicule et renseigne en temps réel sur la position, la vitesse ou la distance parcourue. Il suffit de consulter les informations sur son ordinateur, son PDA, son Blackberry ou son IPhone. L’entrepreneur peut alors optimiser les trajets ou même retrouver un véhicule volé.\r\n\r\nParmi les premiers, le SAMU 44 (Loire-Atlantique) a découvert tous les avantages d’un tel système : lorsqu’il recevait des appels d’urgence, le régulateur du SAMU ne disposait que d’un listing des ambulances disponibles, sans savoir où elles se trouvaient. Il devait alors appeler les voitures une par une car il essuyait de nombreux refus. En effet, comment intervenir d’urgence lorsque l’ambulance se trouve à l’autre bout du département ?\r\n\r\nLa géolocalisation permet désormais de repérer le véhicule le plus proche de la personne à secourir et de réduire ainsi les temps d’intervention. Il est aussi possible de rassurer les malades en leur indiquant précisément le délai d’arrivée des secours. L’intervention est encore optimisée car on peut conseiller au professionnel  le meilleur itinéraire à suivre, en évitant une zone de travaux, par exemple.\r\n\r\nL’expérience de ce service d’urgence nantais a montré l’efficacité de la géolocalisation dans un domaine où chaque seconde compte.\r\n\r\nN’hésitez pas à demander votre devis.', 1, '2018-07-11 16:10:41', '2018-07-11 16:10:41'),
(4, 'Nouvelle levée de fond chez Anoloc', 'nouvelle', 'Issus de fond publics et privés.\r\nLes investissements dans les start-up françaises continuent sur l\'excellente lancée du mois de mai, où elles ont récolté 516 millions d\'euros. Ce mois-ci, les jeunes pousses ont attiré 423 millions d\'investissements, via 77 tours de table. Rien à voir avec juin 2017, où seulement 147 millions d\'euros furent levés. Les secteurs les plus populaires auprès des investisseurs sont le retail, avec 58 millions d\'euros levés, suivi par la fintech (50 millions) et l\'IoT (36 millions).', 1, '2018-07-11 12:22:06', '2018-07-11 15:13:44');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Valentin', 'rocherv.valentin@gmail.com', '$2y$10$OxcWcpG39E0PWMdOpaiLFeK1Ud9dey2qCAb4s8kurxqMFx0i4ayrm', 'KSv4U1cCn0VocJZjQMKfm1VljxcklwzvjL5bU8Le3sMnUxeFG7tTKLajiMa1', '2018-07-09 15:44:12', '2018-07-09 15:44:12');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
