@extends('layout.layout')
@section('title')
 Liste des Articles
@endsection
@section('content')
<div class="flex-center position-ref full-height">

  <div class="container" style="min-height:570px;">
    <h1 class="text-center mt-2 mb-4">Listes des articles <a href="{{route('post.create')}}" class="btn btn-primary">➕ Nouveau</a></h1>
    @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

      @empty($posts)
      <table class="table">
        <thead>
          <th>Id</th>
          <th>Titre</th>
          <th>Contenu</th>
          <th>Créateur</th>
          <th>Actions</th>
        </thead>
        <tbody>
          @foreach($posts as $post)
            <tr>
              <td>{{$post->id}}</td>
              <td>{{$post->title}}</td>
              <td>{{ str_limit( $post->content, 300, '...') }}</td>
              <td>{{$post->user->name}}</td>
              <td>
                <a href="{{route('post.show', ['post' => $post->id])}}" class="btn btn-info m-1">👁</a>
                <a href="{{route('post.edit', ['post' => $post->id])}}" class="btn btn-success m-1">✎</a>
                <a href="{{route('post.destroy', ['post' => $post->id])}}" class="btn btn-danger delete_post m-1">🗑</a>
              </td>
            </tr>
          @endforeach
      </tbody>
    </table>
    @else
    <div class="m-auto text-center">
      <a href="{{route('post.create')}}" class="btn btn-success ">Créer un article pour commencer</a>
    </div>
    @endif
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(function(){
      $(".delete_post").click(function(e) {
        e.preventDefault();
        var href = $(this).attr('href');

       $.ajax({
             url: href,
             method: 'delete',
             headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
             success: function(){
                 alert('Article supprimé');
                 location.reload();
             }
         });
      });
    });
</script>
@endsection
