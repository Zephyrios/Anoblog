@extends('layout.layout')
@section('title')
{{ $post->title }} - Édition
@endsection
@section('content')
<div class="container" style="min-height:570px;">
    {{ Form::open(['route' => ['post.update', $post->id], 'method' => 'put', 'autocomplete' => 'off']) }}
        <div class="row">
            <div class="offset-md-3 col-md-9 col-lg-9">
                <div class="box box-primary">
                    <div class="box-header">
                        <h1 class="box-title">Édition d'un article de blog</h1>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                    {{ Form::label('title', 'Titre') }}
                                    {{ Form::text('title', old('title', $post->title), ['class' => 'form-control', 'autofocus']) }}
                                    {!! $errors->first('title','<p class="text-danger"><strong>:message</strong></p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    {{ Form::label('content', 'Texte') }}
                                    {{ Form::textarea('content', old('content', $post->content), ['class' => 'form-control']) }}
                                    {!! $errors->first('content','<p class="text-danger"><strong>:message</strong></p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-6 offset-md-3 mbl">
                <a href="{{ route("post.show", $post->id) }}" class="btn btn-success">
                    Retour à l'article
                </a>
                <span class="btn-group pull-right">
                    <button type="submit" class="btn btn-primary">
                        Sauver
                    </button>
                </span>
            </div>
        </div>
    {{ Form::close() }}
</div>
@endsection
