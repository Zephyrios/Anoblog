@extends('layout.layout')
@section('title')
{{ $post->title }}
@endsection
@section('content')
<div class="container" style="min-height:570px;">
  <div class="post">
      <h1 class="pb-3">{{ $post->title }}
        @auth
          <a href="{{ route('post.edit', ['post' => $post->id]) }}" class="btn btn-warning">Éditer l'article</a>
        @endauth
      </h1>
      <div class="content" class="pb-3">
        <p>{{ $post->content }}</p>
      </div>
      <hr>
      <p class="text-right">- Par <em>{{ $post->user->name }}</em>, le {{ date('d/m/Y', strtotime($post->created_at)) }}</p>
  </div>
  @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
  @endif
  <div class="comments">
    <h2>Vos réactions <small>({{ $post->comments->count() }} commentaires)</small></h2>
    @if($post->comments)

    @foreach($post->comments as $com)
      <span>De <em>{{ $com->user ? $com->user->name : 'Anonyme' }}</em>, le {{ date('d/m/Y à H:i:s', strtotime($com->created_at)) }}</span>
      @auth
        @if($user->id == $post->user_id)
          <a href="{{ route('comment.destroy', ['comment' => $com->id]) }}" class="delete_comment btn btn-danger btn-sm">Supprimer</a>
        @endif
      @endauth
      <p>{{ $com->content }}</p>
    @endforeach
    @endif
    <hr>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['action' => ['CommentController@store']]) !!}
        <!-- {{ Form::token() }}
      possobilité de suppr par le posteur & dans le mail -->
           {!! Form::hidden('post_id', $post->id) !!}
          @auth
             {!! Form::hidden('user_id', $user->id) !!}
          @endauth
          <div class="form-group">
            {{ Form::label('content', 'Ajouter un commentaire : ') }}
            {{ Form::textarea('content', null, ['class' => 'form-control']) }}
            {{ Form::submit('Ajouter ce commentaire !', ['class' => 'btn btn-info mt-2 ']) }}
        </div>

    {{ Form::close() }}
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(function(){
      $(".delete_comment").click(function(e) {
        e.preventDefault();
        var href = $(this).attr('href');

       $.ajax({
             url: href,
             method: 'delete',
             headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
             success: function(){
                 alert('Commentaire supprimé');
                 location.reload();
             }
         });
      });
    });
</script>
@endsection
