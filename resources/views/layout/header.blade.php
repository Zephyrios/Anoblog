<head>
    <meta charset="UTF-8">
    <title>@yield('title') - Anoblog</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    @yield('styles')
</head>
<body>
<div class="header">
  <nav class="navbar navbar-dark col-lg-6 m-auto">
    <a href="{{ route('home') }}" class="nav-link">Accueil</a>
    <!-- <a href="{{ route('post.index') }}" class="nav-link">Article au hasard</a> -->
    @if (Route::has('login'))
            @auth
                <!-- <a href="{{ route('post.create') }}" class="nav-link">Créer un nouvel article</a> -->
                <a href="{{ route('post.index') }}" class="nav-link">Admin</a>
                <a class="nav-link" href="{{ route('logout') }}"
                  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Déconnexion</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
            @else
                <a href="{{ route('login') }}" class="nav-link">Se connecter</a>
                <a href="{{ route('register') }}" class="nav-link">S'inscrire</a>
            @endauth
    @endif
  </nav>
</div>
