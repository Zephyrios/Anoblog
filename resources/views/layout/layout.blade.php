<!DOCTYPE html>
<html lang="fr">
@include('layout.header')

@yield('content')

@include('layout.footer')
