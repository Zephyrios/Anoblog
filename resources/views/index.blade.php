@extends('layout.layout')
@section('title')
  Blog Laravel par Valentin Rocher
@endsection
@section('content')
<div class="flex-center position-ref full-height">

<div class="container" style="min-height:570px;">
  <h1 class="text-center mt-2 mb-4">Nos derniers articles</h1>
    @empty($posts)
      <div class="offset-md-1 row">
        @foreach($posts as $key => $post)
        <div class="card col-lg-3 m-4" style="min-height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">{{ $post->title }}</h5>
            <p class="card-text">{{ str_limit( $post->content, 100, '...') }}</p>
            <a href="{{ route('post.show', ['post' => $post->id ])}}" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>
        @endforeach
      </div>
    @else
      <p class="text-center">Coming soon</p>
    @endif
</div>
@endsection
