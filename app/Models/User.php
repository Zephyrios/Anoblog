<?php
namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\NewComment as NewCommentNotification;

class User extends Authenticatable
{
  use Notifiable;

  protected $fillable = [ 'name', 'email', 'password', 'remember_token' ];
  protected $hidden = [ 'password', 'remember_token' ];

  /**
   * One to Many relation
   *
   * @return Illuminate\Database\Eloquent\Relations\hasMany
   */
  public function posts()
  {
    return $this->hasMany('App\Models\Post');
  }

  /**
   * One to Many relation
   *
   * @return Illuminate\Database\Eloquent\Relations\hasMany
   */
  public function comments()
  {
    return $this->hasMany('App\Models\Comment');
  }

  public function sendNewCommentNotification($input)
  {
      $this->notify(new NewCommentNotification($input));
  }
}
