<?php
namespace App\Models;
use Eloquent;

class Post extends Eloquent
{
    protected $fillable = [ 'id', 'title', 'slug', 'content', 'user_id' ];

    /**
     * One to Many relation
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function comments()
    {
      return $this->hasMany('App\Models\Comment');
    }
    /**
     * One to Many relation
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
