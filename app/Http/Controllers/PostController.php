<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Auth;
use App\Models\Post;
use Validate;
use Session;

class PostController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();
        if($user !== null){
          return view('post.create', compact('user'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'title' => 'required',
            'content' => 'required'
       ]);
       $input = $request->all();
       $input['slug'] = str_slug($input['title'], '-');
       $post = Post::create($input);

       Session::flash('message', 'Article créé !');

       return redirect()->route('post.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $post = Post::find($id);
        return view('post.show', compact('post','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if($user !== null){
          $post = Post::find($id);
          return view('post.edit', compact('post','user'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
       $request->validate([
            'title' => 'required',
            'content' => 'required'
       ]);
       $input = $request->all();

       $post = Post::find($id);
       $post->update($input);
       Session::flash('message', 'Modification effectuée !');

       return redirect()->route('post.show', $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      Post::destroy($id);
    }

}
