<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Validate;
use Session;

class CommentController extends BaseController {

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'content' => 'required'
       ]);
       $input = $request->all();

       $comment = Comment::create($input);
       Session::flash('message', 'Message reçu !');

       //send notif
       $post = Post::find($input['post_id']);
       $user = User::find($post->user_id);

       $user->sendNewCommentNotification($input);
       return redirect()->route('post.show', $comment->post_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      Comment::destroy($id);
    }

}
